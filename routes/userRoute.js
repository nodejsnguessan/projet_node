const express = require("express");
const router = express.Router();

const { getUsers,getUsersById,deleteUser,createUser,updateUser } = require("../controllers/userController");

router.get("/", getUsers);
router.get("/:id", getUsersById);
router.post("/:id", createUser);
router.delete("/:id", deleteUser);
router.put("/:id", updateUser);
module.exports = router;