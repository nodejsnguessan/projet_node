const connection = require("../config/db");

const getUsers = (req, res) => {
connection.query("SELECT * FROM user", (err, rows) => {
    if (err) throw err;

    res.json(rows);
});
};
const getUsersById = (req, res) => {
    connection.query("SELECT * FROM user WHERE id = ?",[req.params.id], (err, rows) => {
        if (err) throw err;
    
        res.json(rows[0]);
    });
};

const createUser = (req, res) => {
    // connection.query('SELECT MAX(id) AS max_id FROM user',(err, res) =>{
    //     if (err) throw err;
    //     const nextId = results[0].max_id + 1;
    //});
    const newUser = {
        id: req.params.id,
        nom: req.body.nom,
        email: req.body.email,
        password: req.body.password,
      };
    connection.query('INSERT INTO user SET ?', newUser,(err, rows) =>{
        if (err) throw err;
        res.json("l'utilisateur a été bien enregistré");
    });
      
};

const updateUser = (req, res) => {
    const newUse = {
        id: req.params.id,
        nom: req.body.nom,
        email: req.body.email,
        password: req.body.password,
      };
      //[nom, email, password, id]
    connection.query('UPDATE user SET nom=?, email=?, password=? WHERE id=?',newUse, (err, res) => {
        if (err) throw err;
        res.json("Les données de l'utilisateur ont été mises à jour.");
      });
};



const deleteUser = (req, res) => {
    connection.query("DELETE FROM user WHERE id = ?",[req.params.id], (err, rows) => {
        if (err) throw err;
    
        res.json("utilisateur supprimé avec succes");
    });
};

module.exports={getUsers,getUsersById,deleteUser,createUser,updateUser};